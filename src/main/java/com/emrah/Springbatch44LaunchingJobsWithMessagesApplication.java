package com.emrah;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class Springbatch44LaunchingJobsWithMessagesApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springbatch44LaunchingJobsWithMessagesApplication.class, args);
	}

}
